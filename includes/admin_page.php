<?php
require("manifest.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="jakejacobo" content="">


    <link rel="icon" type="image/png" href="../img/samers_icon.ico">
    <title>
        <?php echo $sitename; ?>
    </title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

</head>
<body>




    
<?php
     session_start();
    
    
    if($_SESSION["uid"] == null){
             echo '<script type="text/javascript">
                                alert("Session Expired!, Please Login!");
                                location.href = "logout.php"
                                </script>';
    }else{
    

                        if(!isset($_SESSION["uid"])){
                         header("Refresh:0; url=../index.php ");
                    }


                        $sessionUid  = $_GET['uid'];


                        if($_SESSION["uid"] != $sessionUid){



                                 echo '<script type="text/javascript">
                                                    alert("Unathorized Action!");
                                                    location.href = "logout.php"
                                                    </script>';
                        }
            }
    

    
    
?>
    
    



<!-- ====================   ==========================   HEADER  OPEN =================================================== -->
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
                  <div class="navbar-header page-scroll">                 
                             <a class="navbar-brand" href="../index.php">
                             <span class="FName"><?php echo $sitename; ?></span><br /></a>
                            <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#MyNavBar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>    
                  </div>

                    <div class="collapse navbar-collapse" id="MyNavBar">
                        <ul class="nav navbar-nav navbar-right">
                    
                    <li class="page-scroll">
                        <a href="../index.php"><?php echo $link0;  ?></a>
                    </li>
                    
                      <li class="dropdown page-scroll">
                      <a class="dropdown-toggle" data-toggle="dropdown"><?php echo $job; ?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li ><a href="#postjob"><!-- <?php echo $job_sub1;  ?> --> Post a Job</a></li>
                          <li><a href="#applicantsTable"> Applicants </a></li>
                        </ul>
                      </li>


                

                      <li class="dropdown page-scroll">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> ADMIN <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#CompanyInfo"><!-- <?php echo $Resume;  ?> --> My Company </a></li>    
                          <li><a href="logout.php">Logout</a></li>
                        </ul>
                      </li>

                   </ul>
          </div>
    </div>
</nav>




<!-- ==============================================   HEADER  CLOSE =================================================== -->
<!-- ==============================================   BODY   OPEN =================================================== -->


                          
    <div id="CompanyInfo" class="container-fluid">  <br /><br />  <br /><br />  <br /><br />
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                     <span class="contactTitle">Company Information</span>
                                
                              </div>
                              <div class="panel-body">
                                        <br /><br />
                                  
                                        <?php
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                                            $uid = $_GET['uid'];
                    
                                    $admiQry="SELECT * FROM samerAdmin WHERE uid=$uid";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($dataRow = mysqli_fetch_assoc($resultSet)){    
//                                        echo  $dataRow['uid'];
                                    ?>
                                  
                                  
                                  
                                  
                                       <form class="form-horizontal" role="form" method="post" action="employeerContactInfoRegister.php">
                                       
                                       <input type="hidden" name="uid" value="<?php  echo  $dataRow['uid']; ?>" />
                                           
                                           
                                        <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Name:</label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control" id="employeename" placeholder="Company Name" name="companyname" value="<?php echo  $dataRow['companyname'];?>" required/>
                                          </div>
                                        </div>
                                           
                                        
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="employeerCompanyAddress"> Address:</label>
                                                <div class="col-sm-10">
                 <textarea class="form-control" rows="3" cols="3" id="employeerCompanyAddress" name="employeerCompanyAddress" placeholder="Company Address" required ><?php echo  $dataRow['employeerCompanyAddress'];?></textarea>
                                                             </div>
                                                  </div>   

                                           
                                           
                                               <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeerCompanyContact">Contact No:</label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control" id="employeerCompanyContact" name="employeerCompanyContact" onkeypress='validate(event)' placeholder="Contact Number" value="<?php echo  $dataRow['employeerCompanyContact'];?>" required />
                                           <script type="text/javascript">
                                              function validate(evt) {
                                              var theEvent = evt || window.event;
                                              var key = theEvent.keyCode || theEvent.which;
                                                theEvent.returnValue = false;
                                                if(theEvent.preventDefault) theEvent.preventDefault();
                                              }
                                            }
                                             </script>   

                                          </div>
                                        </div>
                                           
                                        
                                              <div class="form-group">
                                                    <label class="control-label col-sm-2" for="employeerCompanyDetailinformation">Detail Information:</label>
                                                         <div class="col-sm-10">
                                                            <textarea class="form-control" rows="6" cols="1" id="employeerCompanyDetailinformation" name="employeerCompanyDetailinformation" placeholder="Company Detail Information" required >
                                                             
                                                            <?php   echo $dataRow['employeerCompanyDetailinformation'];?>
                                                             </textarea>
                                                             </div>
                                                  </div>   
                                           
                                           
                                           
                                    <div class="form-group">        
                                      <div class="col-sm-12">
                                            <label class="control-label col-sm-2" for="employeeCtact"></label>
                                            <button type="submit" id="saveBtnUser" class="btn btn-success">Save</button>
                                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>
                                        </form>
                                  
                                  
                                  
                                </div>
                             </div>
               
  </div>
       <br />
    
    
    
    <div class="container-fluid" id="applicantsTable">
             <br /><br />
       <div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
               <div class="panel-heading">
                     <span class="contactTitle">Applicants</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">          
                        <table class="table table-bordered table-condensed table-striped">
                           <thead align="center">
                                  <tr>
                                    <th class="tbleApplicant"><h5>#</h5></th>
                                      <th class="tbleApplicant"><h5>NAME</h5></th>
                                      <th class="tbleApplicant"><h5>CONTACT</h5></th>
                                      <th class="tbleApplicant"><h5>DATE APPLY</h5></th>
                                      <th class="tbleApplicant"><h5>JOB  TITLE</h5></th>
                                      <th class="tbleApplicant" colspan="3"><h5></h5></th>
                                  </tr>
                            </thead>
                        <tbody>
                               <?php
                       
                                     require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                                
                                 
                                
                                
                                
                                
                    
                                    $admiQry="SELECT * FROM samerApplicants";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($jobrow = mysqli_fetch_assoc($resultSet)){    
                                       
                                    
                                          
                                ?>
                            
                            <tr class="tableBody">
                                <td><?php echo $jobrow['uid']; ?></td>
                                <td><?php echo $jobrow['applicantName']; ?></td>
                                <td><?php echo $jobrow['applicantEmail']; ?></td>
                                <td><?php echo $jobrow['applicantDateApply']; ?></td>
                                <td><?php echo $jobrow['applicantJobTitle']; ?></td>
                            <td>
                            <a href="employeer_view_specific_applicants.php?applicationUid=<?php echo $jobrow['uid'];?>"
                                    class="btn btn-sm btn-success" data-toggle="modal"><span class="glyphicon glyphicon-search"></span></a>
                            </td>
                            
                          
                        <td><a class="btn btn-sm btn-success" href="admin_page_delete_applicant.php?applicantUid=<?php echo $jobrow['uid']; ?>&uid=<?php echo $uid; ?>"><i class="glyphicon glyphicon-trash"></i></a></td>
     
                        </tr>         
                        </tbody>
                            <?php  } ?>
                         </table>
                    </div>
                                  
               </div>
          </div>
        </div>
     </div>
    </div>
    
    
    
  <div class="container-fluid" id="postjob">
             <br />
       <div class="row">
                      
           
           <div class="col-md-12">
        

                            <div class="panel panel-success">
                              <div class="panel-heading">
                                     <span class="contactTitle">Post Job</span>
                              </div>
                              <div class="panel-body">
                              
                                  
                                       <br /><br />
                                           <form class="form-horizontal" action="employeer_postjob.php" method="post" role="form">
                                  
                                                <input type="hidden" name="uid" value="<?php  echo  $dataRow['uid']; ?>" />
                                    <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Opening Date:</label>
                                          <div class="col-sm-10">
                                        <input class="form-control" type="date" name="employeerJobPostOpenDate" value="<?php echo date('Y-m-d'); ?>" readonly/>
                                          </div>
                                        </div>
                                  
                                
                                        <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Company Name:</label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control" id="employeename" name="employeerJobPostCompanyName" placeholder="Company Name" required />
                                          </div>
                                        </div>
                                     
                                     
                              
                                     
                                         <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Position:</label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control" id="employeename" name="employeerJobPostPosition" placeholder="Position" required />
                                          </div>
                                        </div>
                                               
                                        <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Contact Information:</label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control" id="employeename" name="employeerJobPostContact" placeholder="Phone/Telephone No./Email" required />
                                          </div>
                                        </div>
                                     
                                     
                                         <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Work Description:</label>
                                          <div class="col-sm-10">
                                        
                                              <textarea class="form-control" rows="6" row="3" id="address" placeholder="Descrition/Exprience/Requirements/Etc." name="employeerJobPostExprienceRequired" required></textarea>
                                          </div>
                                        </div>
                                     
                                     
                                        <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Qualification :</label>
                                          <div class="col-sm-10">
                                        <textarea class="form-control" rows="6" row="3" id="address" placeholder="Qualification/Interview/Eximantion Date/Location/Etc." name="employeerJobPostAdditionalInformation" required></textarea>
                                          </div>
                                        </div>
                                     
                                    
                                    <div class="form-group">
                                          <label class="control-label col-sm-2" for="employeename">Closing Date:</label>
                                          <div class="col-sm-10">
                                        <input class="form-control" type="date" name="employeerJobPostCloseDate" value="<?php echo date('m-d-Y'); ?>" />
                                          </div>
                                        </div>
                                               
                                               
                                               
                                   <div class="form-group"> 
                                         <label class="control-label col-sm-2"></label>
                                      <div class="col-sm-10">
                                            <button type="submit" id="btnPostJob" class="btn btn-success">Post</button>
                                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>
                                               
                       
                                  </form>
                                  
                                  
                                  
                                  
                         
                              </div>
                     </div>
         
               
            
           </div>
      </div>
 </div>
    
                           
    
    
     
    
    <?php
        }                              
    ?>
    
    
    
    
    
      
     <div class="container-fluid" id="applicantsTableFeedback">
             <br />
       <div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
               <div class="panel-heading">
                     <span class="contactTitle">Feedback Detail</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">          
                        <table class="table table-bordered table-condensed table-striped">
                           <thead align="center">
                                  <tr>
                                    <th class="tbleApplicant"><h5>#</h5></th>
                                      <th class="tbleApplicant"><h5>NAME</h5></th>
                                      <th class="tbleApplicant"><h5>EMAIL</h5></th>
                                      <th class="tbleApplicant"><h5>CONTACT</h5></th>
                                      <th class="tbleApplicant" colspan="2"><h5>MESSAGE</h5></th>
                                  </tr>
                            </thead>
                        <tbody>
                               <?php
                       
                                     require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                                
                                    
                                    $admiQry="SELECT * FROM samerFeedback";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($feedkRow = mysqli_fetch_assoc($resultSet)){    
                                       
                                    
                                          
                                ?>
                            
                            <tr class="tableBody">
                                <td><?php echo $feedkRow['uid']; ?></td>
                                <td><?php echo $feedkRow['name']; ?></td>
                                <td><?php echo $feedkRow['email']; ?></td>
                                <td><?php echo $feedkRow['phonenumber']; ?></td>
                                <td><?php echo $feedkRow['message']; ?></td>
                                <td><a class="btn btn-sm btn-success" href="admin_page_delete_feedback.php?feedbackUid=<?php echo $feedkRow['uid']; ?>&uid=<?php echo $uid; ?>"><i class="glyphicon glyphicon-trash"></i></a></td>
                            </tr>         
                        </tbody>
                            <?php  } ?>
                         </table>
                    </div>
                                  
               </div>
          </div>
        </div>
     </div>
    </div>
    
    
  


  <?php
require("jscripts.php");
require("includes_js.php");
?>
    


<!-- ==============================================   BODY  CLOSE =================================================== -->


  

</body>

</html>
