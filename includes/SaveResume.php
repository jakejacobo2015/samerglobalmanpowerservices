<?php

require_once("./server_config.php");

//CONNECTION CHECKER 
if(mysqli_connect_errno($MysqlCon)){
	echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
}


//SHOW ALL ERROR REPORTS 
ini_set("error_reporting","true");
error_reporting(E_ALL|E_STRICT);


$uid=$_POST['uid'];
$applicantResumePersonalName = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalName']);
$applicantResumePersonalContact = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalContact']);
$applicantResumePersonalEmail = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalEmail']);
$applicantResumePersonalAddress = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalAddress']);
$applicantResumePersonalBirthDate = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalBirthDate']);
$applicantResumePersonalAge = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalAge']);
$applicantResumePersonalNationality = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumePersonalNationality']);


$applicantResumeEducationSchoolname = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeEducationSchoolname']);
$applicantResumeEducationGradDate = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeEducationGradDate']);
$applicantResumeEducationQualification = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeEducationQualification']);
$applicantResumeEducationFieldStudy = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeEducationFieldStudy']);


$applicantResumeEducationAdditionalinformation = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeEducationAdditionalinformation']);
$applicantResumeWorkSkills = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkSkills']);
$applicantResumeWorkPosition = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkPosition']);
$applicantResumeWorkCompanyName = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkCompanyName']);


$applicantResumeWorkDate = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkDate']);
$applicantResumeWorkRole = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkRole']);


$applicantResumeWorkCountry = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkCountry']);
$applicantResumeWorkState = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkState']);
$applicantResumeWorkIndustry = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkIndustry']);
$applicantResumeWorkExprienceDescription = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeWorkExprienceDescription']);

$applicantResumeAdditionalInfo = mysqli_real_escape_string($MysqlCon, $_POST['applicantResumeAdditionalInfo']);




echo $uid;
echo $applicantResumePersonalName;
echo $applicantResumePersonalContact;
echo $applicantResumeWorkExprienceDescription;


     mysqli_query($MysqlCon,
    "INSERT INTO samerUserResume(	
     applicantResumePersonalName,
     applicantResumePersonalContact,
     applicantResumePersonalEmail,	
     applicantResumePersonalAddress,
     applicantResumePersonalBirthDate,
     applicantResumePersonalAge,
     applicantResumePersonalNationality,
     applicantResumeEducationSchoolname,
     applicantResumeEducationGradDate,
     applicantResumeEducationQualification,	
     applicantResumeEducationFieldStudy,
     applicantResumeEducationAdditionalinformation,
     applicantResumeWorkSkills,	
     applicantResumeWorkPosition,
     applicantResumeWorkCompanyName,
     applicantResumeWorkDate,	
     applicantResumeWorkRole,	
     applicantResumeWorkCountry,
     applicantResumeWorkState,
     applicantResumeWorkIndustry,
     applicantResumeWorkExprienceDescription,
     applicantResumeAdditionalInfo)
VALUES(
'".$applicantResumePersonalName."',
'".$applicantResumePersonalContact."',
'".$applicantResumePersonalEmail."',
'".$applicantResumePersonalAddress."',
'".$applicantResumePersonalBirthDate."',
'".$applicantResumePersonalAge."',
'".$applicantResumePersonalNationality."',
'".$applicantResumeEducationSchoolname."',
'".$applicantResumeEducationGradDate."',
'".$applicantResumeEducationQualification."',
'".$applicantResumeEducationFieldStudy."',
'".$applicantResumeEducationAdditionalinformation."',
'".$applicantResumeWorkSkills."',
'".$applicantResumeWorkPosition."',
'".$applicantResumeWorkCompanyName."',
'".$applicantResumeWorkDate."',
'".$applicantResumeWorkRole."',
'".$applicantResumeWorkCountry."',
'".$applicantResumeWorkState."',
'".$applicantResumeWorkIndustry."',
'".$applicantResumeWorkExprienceDescription."',
'".$applicantResumeAdditionalInfo."')");


header("Location:user_page.php?uid=$uid");


mysqli_close($MysqlCon);
?>