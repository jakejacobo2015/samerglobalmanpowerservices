<!DOCTYPE html>
<html>

<?php
require("manifest.php");
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="SAMERGLOBALMANPOWER" content="">
    <meta name="SAMER GLOBALMANPOWER" content="">


    <link rel="icon" type="image/png" href="../img/samers.png">
    <title>
        <?php echo $sitename; ?>
    </title>

     <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
    <body>

        
        
                                            <?php
        
        
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
        
        $resID = $_GET['resUid'];
        
                 $selectResumeQry="SELECT * FROM samerUserResume WHERE applicantResumePersonalEmail='".$_GET['resUid']."'";
                                            $result=mysqli_query($MysqlCon,$selectResumeQry);
                                            while($rowResume = mysqli_fetch_assoc($result)){
                                                
                                                $name=$rowResume['applicantResumePersonalName'];
                                                
                                        
   ?>
<br /><br /><br />

<div class="container resumemain">
<div class="row">

    <div class="col-md-12 text-center   resumeHeader">
            <br /><br /><br />
        
        <h1><?php echo $rowResume['applicantResumePersonalName']; ?></h1>
                <p class="small contactEmail">
<?php echo $rowResume['applicantResumePersonalContact']; ?> /<?php echo $rowResume['applicantResumePersonalEmail']; ?> <br />
                <?php echo $rowResume['applicantResumePersonalAddress']; ?>
                    
        </p><br />
    </div>
  </div>
    
 <div class="row">  
<div class="col-md-12 text-left   reseumeBodyExprience"><br />
    <h4><span class="glyphicon glyphicon-briefcase"></span> Experience</h4>
        <h6><?php echo $rowResume['applicantResumeWorkPosition']; ?></h6>
<p class="exprience"> 
        <?php echo $rowResume['applicantResumeWorkCompanyName'];?> | <?php echo $rowResume['applicantResumeWorkCountry'];?> | <?php echo $rowResume['applicantResumeWorkState'];?>
<br />
    <?php  echo $rowResume['applicantResumeWorkRole'];?>
    </p>
    
<p class="center-block txtwrkdescription">
    <?php   echo $rowResume['applicantResumeWorkDate']; ?>   -  <?php   echo $rowResume['applicantResumeWorkDate']; ?><br /> 
    <?php  echo $rowResume['applicantResumeWorkExprienceDescription'];  ?>
</p>
</div>
    </div>
    
    
    
<div class="row">     
<div class="col-md-12 text-left   resumeBodyEducation"><br />
        <h4><span class="glyphicon glyphicon-education"></span> EDUCATION</h4>
        <h6><?php echo $rowResume['applicantResumeEducationSchoolname']; ?></h6>
    <br />

    <p class="center-block txtwrkdescription">
 <?php  echo $rowResume['applicantResumeEducationFieldStudy'];?>  | 
 <?php  echo $rowResume['applicantResumeEducationQualification'];?><br />
 <?php   echo $rowResume['applicantResumeEducationGradDate']; ?> <br /> 
 <?php  echo $rowResume['applicantResumeEducationAdditionalinformation'];  ?>
</p>
    </div>
</div>
    
    <div class="row resumeHeader"> 
    

    
    <div class="col-md-12 text-left resumeBodyAbout">    
            <h4>  <span class="glyphicon glyphicon-tasks"></span>  ADDITIONAL INFO</h4>
    <p class="center-block txtwrkdescription">
    <?php  echo $rowResume['applicantResumeAdditionalInfo'];?>
    </p>
        <br/><br/>
    <br /> 
    </div>
</div>
    
    
    
    
<div class="row"> 
        <h4>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span>  ABOUT ME</h4>
    <div class="col-md-3 text-left   resumeBodyAbout"><br />   
        <h7>Date of Birth</h7> <br /><br />
        <h7>Age</h7> <br /><br />
        <h7>Nationality</h7><br /> 
            <br />  
    </div>
    
    
<div class="col-md-6 text-left resumeBodyAbout"><br />              
        <?php  echo $rowResume['applicantResumePersonalBirthDate'];?><br/><br/>
        <?php  echo $rowResume['applicantResumePersonalAge'];?><br/><br/>
        <?php  echo $rowResume['applicantResumePersonalNationality'];?>
    
    <br /> 
    </div>
</div>
    </div>

    
    <?php } ?>
    
        
  
        
        
   

          
<!-- JS / JQry  scripts -->
<?php
require("../includes/jscripts.php");
?>  
 </body>
</html>