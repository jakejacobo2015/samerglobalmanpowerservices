
<?php
require("manifest.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="SAMERGLOBALMANPOWER" content="">
      <meta name="SAMERGLOBALMANPOWER" content="">

    
      <link rel="icon" type="image/png" href="../img/samers.png">
      <title>
          <?php echo $sitename; ?>
      </title>
      <link href="../css/bootstrap.min.css" rel="stylesheet">
    
      <!-- Custom CSS -->
      <link href="../css/freelancer.css" rel="stylesheet">
        
      <!-- Custom Fonts -->
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

</head>
<body>
    
    <?php

       session_start();
    
     if($_SESSION["uid"] == null){
             echo '<script type="text/javascript">
                                alert("User need to Login!");
                                location.href = "../index.php"
                                </script>';
    }
    
    
    ?>
    
    
<?php
require("userpage_jscript.php");
?>  

    

<?php
require("user_navigation.php");
?>

    
    
    
    
<section id="home" class="success">
      <br />
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                        <h2 id="searchJob">Search Jobs</h2>
                        <hr class="star-light">
                </div>
            </div>
<div class="row"  id="usr">
<div class="col-md-2"></div>
        <?php   $uidUser = $_GET['uidUser'];  ?>
<form class="form-horizontal" role="form" action="./index_searchjob_function.php?uidUser=<?php echo $uidUser;?>" method="POST">
    <div class="col-md-8">
        <div class="input-group">
  <input type="text" class="form-control input-lg"  id="searchTXT" name="searchTXT" placeholder="Find Jobs by  Title, Company name  or  Keywords..."  required />
  <span class="input-group-addon">
    <button type="submit" class="btn btn-default" id="search-btn" />
      <span class="glyphicon glyphicon-search"></span>
    </button>
</form>
  </span>
        </div>
            </div>
            <div class="col-md-2"></div>
                </div>
    </div>
</header>
</section>

    
    
    
    
<section id="portfolio">
        <br /> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Available Jobs</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                
                     <div class="table-responsive">          
                        <table class="table table-bordered table-condensed table-striped">
                           <thead align="center">
                                  <tr>
                                    <th class="tbleApplicant hidden"><h5>#</h5></th>
                                      <th class="tbleApplicant"><h5>COMPANY NAME</h5></th>
                                      <th class="tbleApplicant"><h5>POSITION</h5></th>
                                      <th class="tbleApplicant"><h5>CONTACT</h5></th>
                                      <th class="tblLeftApplicant"><h5></h5></th>
                                      
                                  </tr>
                            </thead>
                            <tbody>
                                
                                 <?php
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                                
                                  $uidUser = $_GET['uidUser'];
                               
 
                                    $admiQry="SELECT * FROM samerAdminJobPost";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($dataRow = mysqli_fetch_assoc($resultSet)){    
                                       
                                       $uid  = $dataRow['uid'];
                                      
                                    ?>
                                
                                

                                        <tr class="tableBody">
                                        <td class="hidden"></td>
                                        <td><?php echo $dataRow['employeerJobPostCompanyName'];?>
                                        </td>
                                        <td>
                                        <?php echo $dataRow['employeerJobPostPosition'];
                                        ?>
                                        </td>
                                        <td>
                                         <?php echo $dataRow['employeerJobPostContact'];
                                        ?>
                                        </td>
                                        <td>
                                        <a href="specificJobPost.php?uid=<?php echo $uid;?>&uidUser=<?php echo $uidUser; ?>" class="btn btn-sm btn-success" data-toggle="modal">DETAILS</a>
                                        </td>
                                    </tr>
                                      <?php } ?>

                                  


                            </tbody>
                            </table>
                     </div>
                    
    
                </div>
            </div>
        </div>
    </section>


      
      

   

</body>

</html>
