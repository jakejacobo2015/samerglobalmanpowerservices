<?php


//  MAIN PAGE
$sitename="SAMER Global Manpower Service";





$job ="JOBS";
$job_sub1="SEARCH";
$job_sub2="AVAILABLE JOBS";


$link0 ="Home";

$link1 = "LOGIN";
$link1_sub1 = "APPLICANT";
$link1_sub2 = "EMPLOYER";

$link2="ABOUT";
$link3="CONTACT";




$fullCompanyName = "SAMER GLOBAL <br /> Manpower Services Incorporated";
$fullCompanyName_lowerTxt = "The right man  for the  right place";

$jumbotronImgLink = "./img/samers.png";


$CompanyVision = "To be a leading recruitment, traning and manspower service provider , with national deployment  of skilled Filipino Manpowert
thru developing  and delivering training programs for the Manufacturing,Hospitality,Power and Mining industries to equip with skills to compete,
thrive and develop in today's global workplace.";


$CompanyMission="
Provide opportunities for employment for Filipino  works and at the same time  opportunities for growth advancement and skill enchancement
Uplift the standards and qualifications of Filipino labor through traning and certification to meet the demands and requirements of employment 
worldwide , and in turn , Uplifting the standards of living and well-being of the Filipino worker in particular an Filipino people in general.
Eduate our staff on our culture to  strive always provide the highest level of service to our customers.";


$CompanyCommitment = "
Customer Statisfaction is our main priority in doing a businesss
To achieve customer satisfaction, SAMER GLOBAL Manpower Services Inc. works on establishing trust between our client and within our staff , inline with our goal of being a reliable business partner, with a harmonious working relationship with all our stakeholders. This relationship with our stakeholder coupled with our expertise in the manpower industry allows us to properly position the correct personnel as per requirement.";

$CompanyAdvantage = "
We are strategically located in Olongapo City with a branch office inside the subic bay Freeport Zone in Subic Zambales, which allows  us the flexibility  to provide   manpower services  to both   Freeport and non-Freeport based clients.
\n
Our Industry-specific traning programs provide us with certified skilled workers. Our new partnership with  EMAIACI Philipines, expand our skilled worker  pool to construction, earthmoving and minig related services.

\n
Our Customer services focus allow us to provide related services such ass training,payroll management, temporary staffing and bulk recruitment.

\n
We Have an established performance assessment program that allows us to monitor the effectivity and productivity of our  staff and ensure that we are  within our targets. We provide  a quartely  performance assessment on our manpower pool to make sure that all  are staying  effective and productive based on our clients  requirements.

\n
In line with our commitment to personnel advancement and skills improvment , we provide traning  skills upgrading  at least  twice a year.

\n
We provide internal benefits  to our staff at our  costs  to ensure  that we attrct to  retain  the  best and brightest among  our  personnel pool.";




//login modal

$applicant = "APPLICANT";
$employer = "EMPLOYER";




//  FOR ADMIN PAGE


$admin_MenuLink1 = "JOB POST";
$admin_MenuLink2 = "";
$admin_MenuLink3 = "ADMINISTRATOR";
$admin_MenuLink4 = "USERS";
$admin_MenuLink5 = "Setting";



//for user page
$profile =  "MyAccount";
$Resume = "My Resume";
$MyAccount = "My Account";
$Logout = "Logout";

$ContactInfo = "Contact Information";
$ChangePass = "Change Password";
$MyResume = "Resume";
$Edit = "Edit";



$terms="
Samer Global Manpower Services Inc. 
provides the http://www.samerglobalmanpowerservices.com site 
and various related services (collectively, the site) to you, 
the user, subject to your compliance with all the terms, conditions, 
and notices contained or referenced herein (the Terms of Use), as 
well as any other written agreement between us and you. In addition, 
when using particular services or materials on this site, users 
shall be subject to any posted rules applicable to such services 
or materials that may contain terms and conditions in addition 
to those in these Terms of Use.

All such guidelines or rules 
are hereby incorporated by reference into these Terms of Use.
BY USING THIS SITE, YOU AGREE TO BE BOUND BY THESE TERMS OF USE. 
IF YOU DO NOT WISH TO BE BOUND BY THE THESE TERMS OF USE, PLEASE 
EXIT THE SITE NOW. YOUR REMEDY FOR DISSATISFACTION WITH THIS SITE,
 OR ANY PRODUCTS, SERVICES, CONTENT, OR OTHER INFORMATION AVAILABLE 
ON OR THROUGH THIS SITE, IS TO STOP USING THE SITE AND/OR THOSE 
PARTICULAR PRODUCTS OR SERVICES. YOUR AGREEMENT WITH US REGARDING 
COMPLIANCE WITH THESE TERMS OF USE BECOMES EFFECTIVE IMMEDIATELY
UPON COMMENCEMENT OF YOUR USE OF THIS SITE.

As used in these Terms of Use, references to our Affiliates 
include our owners, subsidiaries, affiliated companies, officers, 
directors, suppliers, partners, sponsors, and advertisers, and 
includes (without limitation) all parties involved in creating, 
producing, and/or delivering this site and/or its contents.
";



?>