
<?php
require("manifest.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="SAMERGLOBALMANPOWER" content="">
      <meta name="SAMERGLOBALMANPOWER" content="">

    
      <link rel="icon" type="image/png" href="../img/samers.png">
      <title>
          <?php echo $sitename; ?>
      </title>
      <link href="../css/bootstrap.min.css" rel="stylesheet">
    
      <!-- Custom CSS -->
      <link href="../css/freelancer.css" rel="stylesheet">
        
      <!-- Custom Fonts -->
      <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


      <?php
          require("captcha.php");
      ?>  
</head>
<body>

<?php
     session_start();
    
        
    if($_SESSION["uid"] == null){
             echo '<script type="text/javascript">
                                alert("Session Expired!, Please Login!");
                                location.href = "logout.php"
                                </script>';
    }else{
        
                    if(!isset($_SESSION["uid"])){
                     header("Refresh:0; url=../index.php ");
                }


                    $sessionUid  = $_GET['uid'];


                    if($_SESSION["uid"] != $sessionUid){



                             echo '<script type="text/javascript">
                                                alert("Unathorized Action!");
                                                location.href = "logout.php"
                                                </script>';
                    }
               }
        
?>
    
    
    
<!-- JS / JQry  scripts -->
<?php
require("userpage_jscript.php");
?>  


<!--  NAVIGATION MODULE -->

<?php
require("user_navigation.php");
?>



<!-- PANEL  CONTACT INFORMATION -->
  

    
    

  <div class="container-fluid">
             
       <div class="row">

              <div class="col-md-12">
                 <div class="panel-group">

                  <!-- PANEL  FOR CONTANT INFO -->
               
                     <div id="contactSection">
                          <br /><br /><br /><br/><br /><br />
              
             
                         
                         
    <div class="alert alert-danger" id="newUserAlert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Hello!</strong> Please Fillup the following information details. Thank you!
  </div> 
                       
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                     <span class="contactTitle"><?php  echo $ContactInfo; ?></span>
                            
                              </div>

                              <div class="panel-body">
                                    
                               
                                  <?php
                                        
                    
                                 require("server_config_li.php");
                            
                                    $sql_query="SELECT * FROM samerUser WHERE uid='".$_GET['uid']."'";
                                    $result_set=mysql_query($sql_query);
                                    while($row = mysql_fetch_array($result_set)){                                             
                                    ?>
                                  
                                  
                                    

                                
                                  <form class="form-horizontal" role="form" method="post" action="user_registercontact.php">

                                        <input type="hidden"  name="uid" value="<?php echo $row['uid'];?>">
                                
                                        <div class="form-group">
                                          <label class="control-label col-sm-1" for="employeename">Name:</label>
                                          <div class="col-sm-6">
                                            <input type="text" class="form-control" id="applicantName" name="applicantName" placeholder="Enter Fullname" value="<?php echo $row['applicantName'];?>" required />
                                          </div>
                                        </div>

                                       <div class="form-group">
                                          <label class="control-label col-sm-1" for="employeeEmail">Email:</label>
                                          <div class="col-sm-6">
                                            <input type="text" class="form-control" id="applicantEmail" name="applicantEmail" placeholder="Enter email" value="<?php echo $row['username'];?>"  required />
                                          </div>
                                        </div>
                                                                    
                                       <div class="form-group">
                                          <label class="control-label col-sm-1" for="employeeCtact">Contact No:</label>
                                          <div class="col-sm-6">
                                            <input type="text" class="form-control" id="applicantContact" name="applicantContact" placeholder="Enter Contact Number" value="<?php echo $row['applicantContact'];?>" onkeypress='validate(event)'  required />
                                          </div>
                                        </div>


                                      <div class="form-group">
                                        <label class="control-label col-sm-1" for="address">Address :</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" rows="2" row="3" id="applicantAddress" name="applicantAddress" placeholder="Enter Address" required ><?php echo $row['applicantAddress'];?></textarea>
                                        </div>
                                      </div>


                                    <div class="form-group">        
                                      <div class="col-sm-6">
                                            <label class="control-label col-sm-2" for="employeeCtact"></label>
                                            <button type="submit" id="saveBtnUser" class="btn btn-success">Save</button>
                                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>

                                  </form>
                                
                              </div>
                            </div>
                        </div>
                        <br />

                        <div id="changepassSection">
                           <div class="panel panel-success">
                              <div class="panel-heading">
                                     <span class="contactTitle"><?php  echo $ChangePass; ?></span>
                                    
                              </div>
                                    <div class="panel-body">
                                     <form class="form-horizontal" role="form" method="post" action="user_update_pass_function.php">

                                        <input type="hidden" name="uid" value="<?php echo $row['uid'];?>"  />
                            
                                         
                                         
                                      <div class="form-group">
                                        <label class="control-label col-sm-1" for="password">Old Password:</label>
                                        <div class="col-sm-6">
                                          <input type="password" class="form-control" id="password" value="<?php echo $row['password'];?>" name="oldpassword" required/>
                                        </div>
                                      </div>

                                         
                                    <div class="form-group">
                                        <label class="control-label col-sm-1" for="password">New Password:</label>
                                        <div class="col-sm-6">
                                          <input type="password" class="form-control" id="password" name="newpassword" placeholder="New Password" required/>
                                        </div>
                                      </div>

                                         
                                    <div class="form-group">        
                                      <div class="col-sm-6">
                                            <label class="control-label col-sm-2" for="employeeCtact"></label>
                                            <button type="submit" id="saveBtnUser" class="btn btn-success">Update</button>
                                    </div>
                                    </div>
                                     </form>
                                    </div>
                           </div>
                        </div>
                     
                    


                             
                             
<!--==================================================  RESUME SECTION  ===================================================== -->
                     <div id="resumeSection"> 
                         <br /><br /><br /><br /><br /><br />
                    <div class="panel panel-success">
                              <div class="panel-heading">
                                     <span class="contactTitle"><?php  echo $MyResume; ?></span>
                              </div>
                
                               
                <div class="panel-body">
            
                    
                    <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#resume1">View Resume</a></li>
                    <li><a data-toggle="tab" href="#resume2">Upload Resume</a></li>
                    <li><a data-toggle="tab" href="#resume3">Create Resume</a></li>
                  </ul>

                     <div class="tab-content">
                        <div id="resume1" class="tab-pane fade in active">
                            
                            
                             
                
                                                    <?php
                                                    require("user_viewresume.php");
                                                    ?> 
                        </div>
                         
                       <div id="resume2" class="tab-pane fade">
                        
                
                       <form action="user_upload_resume_function.php" method="post" enctype="multipart/form-data">
                          <br />
                          
                           <input type="hidden" value="<?php echo $_GET['uid'] ?>" name="uid" />
                           
                           <div class="uploadObj">
                                <input type="file" name="file" class="btn btn-primary" />
                            </div> 
                             <div class="uploadObj">
                    <button type="submit" name="btn-upload" class="btn btn-success">
                                    <span class="glyphicon glyphicon glyphicon-upload"></span>
                                      UPLOAD
                                </button>
                                 </div> 
                            
                        </form>

                           
                           
                           
                           
                           
                        </div>
                         
                         
                        <div id="resume3" class="tab-pane fade">
                        
                                 <?php
                                     require("user_createresume.php");
                                    ?>
                        </div>            

                         
                        </div>
                    </div>
                    
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />  
                             <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /> 
                </div>
                                                
                                         
                                </div>
                     
                            </div>
                 
                  
                  </div>
              </div>

       </div>
  </div>

    <?php } ?>



</body>

</html>
