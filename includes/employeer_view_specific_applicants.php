<!DOCTYPE html>
<html>

<?php
require("manifest.php");
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="SAMERGLOBALMANPOWER" content="">
    <meta name="SAMER GLOBALMANPOWER" content="">


    <link rel="icon" type="image/png" href="../img/samers.png">
    <title>
        <?php echo $sitename; ?>
    </title>

     <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
    <body>

  
    <?php
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                           
        
                        $applicationUid=$_GET['applicationUid'];
        
                                $admiQry="SELECT * FROM samerApplicants WHERE uid=$applicationUid";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($dataRow = mysqli_fetch_assoc($resultSet)){   
        
        
                                        $resumeUID=$dataRow['uid'];
                                        $resumeEmail = $dataRow['applicantEmail'];
                                    ?>

<div class="container"><br /><br />
          <div class="panel panel-success">
                <div class="panel-heading">
                      <h3><?php  echo $dataRow['applicantName'];?></h3>
                </div>


                <div class="panel-body">  
                        <div class="row">
                               <div class="col-md-6">
                                     <h4>Email:</h4>
                                        <h4><?php  echo $dataRow['applicantEmail'];?></h4>
                               </div>

                         </div>
                    
                    <br />
                      <div class="row">
                                <div class="col-md-8">
                                <h4>Date Apply:</h4>
                                    <p class="jobDescription center-block"><?php echo $dataRow['applicantDateApply'];?></p>             
                                </div>
                        </div>
                      
                    
                        <div class="row">
                                <div class="col-md-8">
                                <h4>Detail:</h4>
                                    <p class="jobDescription center-block">  <?php echo $dataRow['applicantDetail'];?></p>
                                    
                                </div>
                        </div>
                   
                    <?php
                    
                                        $appUid = $dataRow['applicantUid'];
                                        
                                        
                                                        require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);

        
                                            $selectResumeQry="SELECT * FROM resumeDocs WHERE uid=$appUid";
                                            $result=mysqli_query($MysqlCon,$selectResumeQry);
                                            while($rowResume = mysqli_fetch_assoc($result)){
                                                
                                                $fileName =$rowResume['file'];
                                          
                                            }
                                            
                                        
//                                        echo $fileName;
                             
              
                    ?>
                
                                  
        
                            <?php } ?>
                    

                 </div>
                <div class="panel-footer">
                    <div class="pull-right">
<a href="employeer_view_specific_resume.php?resUid=<?php echo $resumeEmail;?>" target="_blank" class="btn btn-success btn-md" value="Apply">VIEW RESUME
                        </a>
                        
                    
                        
<a href="uploads/<?php echo $fileName; ?>" target="_blank" class="btn btn-success btn-md" download>DOWNLOAD RESUME </a>
<!--                        <a href="../index.php" class="btn btn-md btn-danger">BACK</a>-->
                    </div>
                    <br /><br />
                </div>
          </div>
</div>



          
<!-- JS / JQry  scripts -->
<?php
require("../includes/jscripts.php");
?>  
 </body>
</html>