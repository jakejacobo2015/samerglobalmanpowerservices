    <br />
  <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div classn="footer-col col-md-4">
                   
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/samerglobalmanpowerservicesinc/timeline" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <!-- <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a> -->
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>HOTLINE </h3>
                        <p> Telephone: (047) 222-4067 / Mobile:09234524292 </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; SAMER Global Manpower Services Inc.
                    </div>
                </div>
            </div>
        </div>
    </footer>