<nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
       
                  <div class="navbar-header page-scroll">                 
                             <a class="navbar-brand" href="#header">
                             <span class="FName"><?php echo $sitename; ?></span><br /></a>
                            <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#MyNavBar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>    
                  </div>

                    <div class="collapse navbar-collapse" id="MyNavBar">
                        <ul class="nav navbar-nav navbar-right">
                    
                    <li class="page-scroll active">
                        <a href="#header"><?php echo $link0;  ?></a>
                    </li>
                    
                      <li class="dropdown page-scroll">
                      <a class="dropdown-toggle" data-toggle="dropdown"><?php echo $job; ?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#home"><?php echo $job_sub1;  ?></a></li>
                          <li><a href="#portfolio"><?php echo $job_sub2 ?></a></li>
                        </ul>
                      </li>

                            
                     <li class="dropdown page-scroll">
                      <a class="dropdown-toggle" data-toggle="dropdown">Login<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a data-toggle="modal" data-target="#applicantModal" href="#">Applicant</a></li>
                          <li><a data-toggle="modal" data-target="#employerLoginModal" href="#" >Employeer</a></li>
                        </ul>
                      </li>

<!--
                    <li class="page-scroll">
                        <a data-toggle="modal" data-target="#applicantModal" href="#"><?php echo $link1;  ?></a>
                    </li>
-->

                     <li class="page-scroll">
                        <a href="#about"><?php echo $link2;  ?></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact"><?php echo $link3;  ?></a>
                    </li>
                   </ul>
          </div>
    </div>
</nav>