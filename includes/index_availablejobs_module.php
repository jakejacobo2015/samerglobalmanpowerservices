<!-- AVAILABLE JOBS -->
    <section id="portfolio">
        <br /> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Available Jobs</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                
                     <div class="table-responsive">          
                        <table class="table table-bordered table-condensed table-striped">
                           <thead align="center">
                                  <tr>
                                    <th class="tbleApplicant hidden"><h5>#</h5></th>
                                      <th class="tbleApplicant"><h5>COMPANY NAME</h5></th>
                                      <th class="tbleApplicant"><h5>POSITION</h5></th>
                                      <th class="tbleApplicant"><h5>CONTACT</h5></th>
                                      <th class="tblLeftApplicant"><h5></h5></th>
                                      
                                  </tr>
                            </thead>
                            <tbody>
                                
                                 <?php
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                                
                                
                                
                                
                                
                                
                    
                                    $admiQry="SELECT * FROM samerAdminJobPost";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($dataRow = mysqli_fetch_assoc($resultSet)){    
                                       
                                    
                                    ?>
                                

                                        <tr class="tableBody">
                                        <td class="hidden"></td>
                                        <td><?php echo $dataRow['employeerJobPostCompanyName'];?>
                                        </td>
                                        <td>
                                        <?php echo $dataRow['employeerJobPostPosition'];
                                        ?>
                                        </td>
                                        <td>
                                         <?php echo $dataRow['employeerJobPostContact'];
                                        ?>
                                        </td>
                                  
                                        <td>
                                        <a href="./includes/admin_specific_applicant.php?uid=<?php echo $dataRow['uid'];?>" class="btn btn-sm btn-success" data-toggle="modal">
                                        DETAILS
                                        </a>
                                        </td>
                                    </tr>
                                      <?php } ?>

                                  


                            </tbody>
                            </table>
                     </div>
                    
    
                </div>
            </div>
        </div>
    </section>