<form class="form-horizontal" method="post" action="SaveResume.php" role="form">
                 <input type="hidden" value="<?php echo $row['uid'];  ?>" name="uid" />
             

<!--=====================================  ABOUTS SECTION  =================================== -->           
<div class="panel panel-default">             
                    <div class="panel-heading"><h4>About Me</h4></div>
                    <div class="panel-body"><br />
                                   <div class="form-group">
                                            <span class="col-sm-1"></span>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control"  id="fullname" placeholder="Fullname" name="applicantResumePersonalName"  required />
                                     </div>
                                </div>
                                   <div class="form-group">
                                                       <span class="col-sm-1"></span>
                                                         <div class="col-sm-10">
                                                <input type="text" class="form-control" id="ContactNO" placeholder="Contact No" name="applicantResumePersonalContact" onkeypress='validate(event)' required /> 
                                                               


                                                                                                </div>
                                                                                            </div>
                                   <div class="form-group">
                                             <span class="col-sm-1"></span>
                                         <div class="col-sm-10">
                                          <input type="email" class="form-control" id="Email" placeholder="Email"
                                                 name="applicantResumePersonalEmail" value="<?php echo $row['username'];?>"
                                                 readonly/>
                                                                                                </div>
                                                                                            </div>
                                    <div class="form-group">
                                                <span class="col-sm-1"></span>
                                             <div class="col-sm-10">
                                                 <input type="text" class="form-control" id="Address" placeholder="Address"    name="applicantResumePersonalAddress" required/>
                                                                                                </div>
                                                                                            </div>

                                    <div class="form-group">
                                                    <span class="col-sm-1"></span>
                                                     <div class="col-sm-10">     
                                                        <input  type="date" class="form-control" id="birthdate" name="applicantResumePersonalBirthDate"  required/>
                                                                                                </div>
                                                                                           </div> 

                                    <div class="form-group">
                                                <span class="col-sm-1"></span>
                                                        <div class="col-sm-10">
                                         <input type="text" class="form-control" id="age" placeholder="Age"
                                         name="applicantResumePersonalAge" onkeypress='validate(event)'  required />
                                                                                                </div>
                                                                                            </div>
                        <div class="form-group">
                                             <span class="col-sm-1"></span>
                                                      <div class="col-sm-10">
                                                     <input type="text" class="form-control" id="Nationality" placeholder="Nationality" name="applicantResumePersonalNationality"/>
                                                                                    </div>
                                                                                </div>   
                                                                         </div>
</div>

                                              

 <!--===============================================  EDUCATION SECTION  ===================================================== --> <div class="panel panel-default">
                                                         <div class="panel-heading"><h4>Education</h4></div>
                                                           <div class="panel-body">
                                                               

                                                                        <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                            <input type="text" class="form-control" id="schoolname" placeholder="Institute/University" name="applicantResumeEducationSchoolname"/>
                                                                          </div>
                                                                        </div>


                                                    <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                            <input type="month" class="form-control" id="gradDate"   name="applicantResumeEducationGradDate"/>
                                                                          </div>
                                                                        </div>

                                             <div class="form-group">
                                                            <span class="col-sm-1"></span>
                                                                <div class="col-sm-10">
                                                           <label for="sel2"></label>
                                        <select  class="form-control" id="sel2" name="applicantResumeEducationQualification">
                                                                            
                                                                                <option selected="selected">High School Diploma</option>
                                                                                <option>Vocational Diploma / Short Course Certificate</option>
                                                                                <option>Bachelor's/College Degree</option>
                                                                                <option>Post Graduate Diploma / Master's Degree</option>
                                                                                <option>DoctorateDegree</option>
                                                                              </select>
                                                                          </div>
                                                                        </div>

                                                                          <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                 <input type="text" class="form-control" id="txtFieldStudy" placeholder="Field of Study"
          name ="applicantResumeEducationFieldStudy"/>
                                                                          </div>
                                                                        </div>
                                                                        
                                                         <div class="form-group">
                                                                                    <span class="col-sm-1"></span>
                                                                                <div class="col-sm-10">
      <textarea class="form-control" rows="6" row="3" id="address" placeholder="Other Information" name="applicantResumeEducationAdditionalinformation"></textarea>
                                                                                </div>
                                                                              </div>  
                                                           </div>
</div>


                                             
                                                   
                            
<!--==================================================  SKILLS SECTION  ===================================================== -->                                                                                               
 <div class="panel panel-default">
                                                         <div class="panel-heading"><h4>SKILLS</h4></div>
                                                           <div class="panel-body">
                                                                		         <div class="form-group">
                                                                                    <span class="col-sm-1"></span>
                                                                                <div class="col-sm-10">
                                                              <textarea class="form-control" rows="6" row="3" id="address" placeholder="Anything about yours skills" name="applicantResumeWorkSkills"></textarea>
                                                                                </div>
                                                                              </div>   
                                                            </div> 
</div>
                                        


                                                   
                                                   
<!--====================================EXPRIENCE SECTION======================================== -->   
    <div class="panel panel-default">
                                                         <div class="panel-heading"><h4>EXPRIENCE</h4></div>
                                                           <div class="panel-body">
                                                               
                                                                        <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                <input type="text" class="form-control" id="txtpositionTitle" placeholder="Position/Title" name="applicantResumeWorkPosition"/>
                                                                          </div>
                                                                        </div>
                                                                		  

                                                                        <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                            <label for="sel2"></label>
<input type="text" class="form-control" id="txtcompanyName" placeholder="Company Name"
                             name="applicantResumeWorkCompanyName"/>
                                                                          </div>
                                                                        </div>
                                                                          

                                                                        <div class="form-group">
                                                                     <span class="col-sm-1"></span>
                                                                          <div class="col-sm-5">
                                                                         <label for="sel2"></label>
      <input type="month" class="form-control" id="startDate"  name="applicantResumeWorkDate"/>
                                                                          </div>

                                                                            <div class="col-sm-5">
                                                                             <label for="sel2"></label>
                                <input type="month" class="form-control" id="endDate"  name="endDate"/>
                                                                          </div>
                                                                    
                                                                        </div>


                                                                          </div>
                                                                   


                                                                        <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                             <label for="sel2"></label>
<!--
                         <input type="text" class="form-control" id="txtcompanyName"name="applicantResumeWorkRole" placeholder="Company Role" />
                                                                              
-->
                     <textarea class="form-control" rows="3" row="3" id="address" placeholder="Company Role Description" name="applicantResumeWorkRole"></textarea>                      
                                                        
                                                                              
                                                                          </div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                           <label for="sel2"></label>
                        <select  class="form-control" id="sel2" name="applicantResumeWorkCountry">
                                                                                <option selected="selected">QATAR</option>
                                                                                <option>USA</option>
                                                                                <option>Philippines</option>
                                                                                <option>Canada</option>
                                                                                <option>Saudi Arabia</option>
                                                                                <option>Japan</option>
                                                                              </select>
                                                                          </div>
                                                                        </div>


                                                                            <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                           <label for="sel2"></label>
                                                                              <select  class="form-control" id="sel2" name="applicantResumeWorkState">
                                                                                <option selected="selected">CENTRAL LUZON</option>
                                                                                <option>VISAYA</option>
                                                                                <option>MINDANAO</option>
                                                                    
                                                                              </select>
                                                                          </div>
                                                                        </div>    



                                                                          <div class="form-group">
                                                                         <span class="col-sm-1"></span>
                                                                          <div class="col-sm-10">
                                                                           <label for="sel2"></label>
                                                                              <select  class="form-control" id="sel2" name="applicantResumeWorkIndustry">
                                                                                <option selected="selected">NGO</option>
                                                                                <option>Goverment / Defence</option>
                                                                                <option>Account / Audit / Tax / Tax Service</option>
                                                                                <option>Advertising / Marketing / Promotion / PR</option>
                                                                    
                                                                              </select>
                                                                          </div>
                                                                        </div>


             <div class="form-group">
                            <span class="col-sm-1"></span>
                     <div class="col-sm-10">
                  <label for="sel2"></label>
                <textarea class="form-control" rows="6" row="3" id="address" placeholder="Experience Description" name="applicantResumeWorkExprienceDescription"></textarea>
                </div>
                  </div>   
                    </div>
                                             


                                                           
<!--===================================ADDITIONAL ========================================= -->    
<div class="panel panel-default">
         <div class="panel-heading"><h4>Addtional Information</h4></div>
         <div class="panel-body">
                                 
<div class="form-group">
                    <span class="col-sm-1"></span>
                <div class="col-sm-10">
      <textarea class="form-control" rows="6" row="3" id="address" placeholder="Enter your academic  details and achievements. Example: courses and subject  taken, extra-curricular activities,projects,detials  of thesis,awards/presizes won,etc." name="applicantResumeAdditionalInfo"></textarea>
                    </div>
</div>  
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
      </div>                                        
</div>                  
<!--===================================  SAVE SECTION  ===================================== -->  

 <div class="form-group">
<br /><br />
                                                      	<div class="col-sm-2"></div>
                                                            <div class="col-sm-4">
                                                             	<input type="submit" class="btn btn-success btn-lg btn-block" value="Save">        		
                                                            </div>


                                                             	<div class="col-sm-4">
                                                                    <button type="button" class="btn btn-danger btn-lg btn-block">Cancel</button>    
                                                                </div>
                                                                    <div class="col-sm-2"></div>
                        </div>
                        
  
</form>
   
               <script type="text/javascript">

  function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

                                                    </script>                              