<!DOCTYPE html>
<html>

<?php
require("manifest.php");
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="SAMERGLOBALMANPOWER" content="">
    <meta name="SAMER GLOBALMANPOWER" content="">


    <link rel="icon" type="image/png" href="../img/samers.png">
    <title>
        <?php echo $sitename; ?>
    </title>

     <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
    <body>

  
                              <?php
        
                                           require_once("server_config.php");
                        
                                            if(mysqli_connect_errno($MysqlCon)){
                                                echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
                                            }

                                            //SHOW ALL ERROR REPORTS 
                                            ini_set("error_reporting","true");
                                            error_reporting(E_ALL|E_STRICT);

                                            //TIMEZONE SETTING:
                                            date_default_timezone_set($timezone);
                           
                                    $uid=$_GET['uid'];
                                    $admiQry="SELECT * FROM samerAdminJobPost WHERE uid=$uid";
                                    $resultSet=mysqli_query($MysqlCon,$admiQry);
                                    while($dataRow = mysqli_fetch_assoc($resultSet)){    
                                      
                                  ?>

<div class="container"><br /><br />
          <div class="panel panel-success">
                <div class="panel-heading">
                      <h3><?php  echo $dataRow['employeerJobPostPosition'];?></h3>
                </div>


                <div class="panel-body">  
                        <div class="row">
                               <div class="col-md-6">
                                     <h5>Company Name:</h5>
                                        <h4><?php  echo $dataRow['employeerJobPostCompanyName'];?></h4>
                               </div>

                         </div><br />
               

                        <div class="row">
                                <div class="col-md-8">
                                <h4>Work Description:</h4>
                                    <p class="jobDescription center-block"><?php echo $dataRow['employeerJobPostExprienceRequired'];?></p>             
                                </div>
                        </div>
                    
                    <br />
                      <div class="row">
                                <div class="col-md-8">
                                <h4>Qualification Detail:</h4>
                                    <p class="jobDescription center-block"><?php echo $dataRow['employeerJobPostAdditionalInformation'];?></p>             
                                </div>
                        </div>
                      
                    
                        <div class="row">
                                <div class="col-md-8">
                                <h4>Contact:</h4>
                                    <p class="jobDescription center-block">  <?php echo $dataRow['employeerJobPostContact'];?></p>
                                    
                                </div>
                        </div>
                    
                       <div class="row">
                                <div class="col-md-8">
                                <h4>Closing Date:</h4>
                                    <p class="jobDescription center-block">    <?php  echo $dataRow['employeerJobPostCloseDate'];?></p>
                                </div>
                        </div>
                    

                 </div>
                <div class="panel-footer">
                    <div class="pull-right">
<!--                        <a href="#" class="btn btn-md btn-success"  data-toggle="modal" data-target="#applyModal">Apply</a> -->
                        <button type="button" class="btn btn-success btn-md" data-toggle="modal" value="Apply" data-target="#applyModal">
                            Apply Now
                        </button>
                        <a href="../index.php#portfolio" class="btn btn-md btn-danger">Cancel</a>
                    </div>
                    <br /><br />
                </div>
          </div>
</div>



  <!-- apply Modal -->
  <div class="modal fade" id="applyModal" role="dialog">
      <br/><br /><br />
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Make your self great to the employeer.</h4>
        </div>
        <div class="modal-body">
           <form class="form-horizontal" method="post" action="user_applyjobs.php" role="form">
        <div class="row">
            
            <div class="col-md-12">
               
                        
                        <div class="form-group">
                            <textarea class="form-control" rows="10" row="3" id="address"  name="applicantDetail"  placeholder="Tell the employer why you are best suited for this role. Highlight specific skills and how you can contribute. Avoid generic pitches e.g I am responsible." required></textarea>
                        </div>
                
                <input type="hidden" name="applicantJobTitle" value="<?php echo $dataRow['employeerJobPostPosition'];?>" />
                <input type="hidden" name="applicantJobUid"  value="<?php echo $dataRow['uid']; ?>" />
                <input type="hidden"  value="<?php echo $_GET['uidUser'];?>" name="userUid" />
                   
                   
                
           
   </div>
        </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Submit Application</button>    
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
             </form>
      </div>
    </div>
  </div>

 <?php } ?>

          
<!-- JS / JQry  scripts -->
<?php
require("../includes/jscripts.php");
?>  
 </body>
</html>