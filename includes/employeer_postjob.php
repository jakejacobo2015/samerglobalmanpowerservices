<?php

require_once("./server_config.php");

//CONNECTION CHECKER 
if(mysqli_connect_errno($MysqlCon)){
	echo "Failed to Connect in MYSQL SERVER" . mysqli_connect_errno();
}


//SHOW ALL ERROR REPORTS 
ini_set("error_reporting","true");
error_reporting(E_ALL|E_STRICT);

//TIMEZONE SETTING:
date_default_timezone_set($timezone);


$uid=$_POST['uid'];

$employeerJobPostOpenDate = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostOpenDate']);
$employeerJobPostCompanyName = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostCompanyName']);
$employeerJobPostPosition = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostPosition']);
$employeerJobPostContact = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostContact']);
$employeerJobPostExprienceRequired = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostExprienceRequired']);
$employeerJobPostAdditionalInformation = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostAdditionalInformation']);
$employeerJobPostCloseDate = mysqli_real_escape_string($MysqlCon, $_POST['employeerJobPostCloseDate']);


mysqli_query($MysqlCon,"INSERT INTO samerAdminJobPost (employeerJobPostOpenDate,
employeerJobPostCompanyName,
employeerJobPostPosition,
employeerJobPostContact,
employeerJobPostExprienceRequired,
employeerJobPostAdditionalInformation,
employeerJobPostCloseDate)
    VALUES('".$employeerJobPostOpenDate."',
    '".$employeerJobPostCompanyName."',
    '".$employeerJobPostPosition."',
    '".$employeerJobPostContact."',
    '".$employeerJobPostExprienceRequired."',
    '".$employeerJobPostAdditionalInformation."',
    '".$employeerJobPostCloseDate."'
    )");
    


echo "<script> alert('Job post Created!'); window.location.href='admin_page.php?uid=$uid'</script>";



?>