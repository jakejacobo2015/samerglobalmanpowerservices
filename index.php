<!DOCTYPE html>
<html lang="en">

<?php
require("./includes/manifest.php");
?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="SAMERGLOBALMANPOWER" content="">
    <meta name="SAMER GLOBALMANPOWER" content="">


    <link rel="icon" type="image/png" href="./img/samers.png">
    <title>
        <?php echo $sitename; ?>
    </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


</head>






<body>

    <?php

   session_start();
   unset($_SESSION['username']);
   unset($_SESSION['passsword']);
   unset($_SESSION['uid']);




    ?>


<!--  NAVIGATION MODULE -->
<?php
require("./includes/index_navigation_module.php");
?>





<!--  MAIN  HEADER -->
<?php
require("./includes/index_header_module.php");
?>





<!-- ABOUT US -->

<?php
require("./includes/index_aboutus_module.php");
?>





<!--  AVAILABLE JOBS -->
<?php
require("./includes/index_availablejobs_module.php");
?>





<!-- SEARCH JOB TEXTBOX -->
<?php
require("./includes/index_searchjob_textbox.php");
?>





    <!-- Contact Section -->
<?php
require("./includes/index_contact_module.php");
?>







    <br />
    <!-- Footer -->
<?php
require("./includes/index_footer_module.php");
?>


    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>








<!--APPLICANT  LOGIN MODAL -->
<?php
require("./includes/index_login_modal.php");
?>





<!-- EMPLOYEER LOGIN MODAL -->
<?php
require("./includes/index_login_employeer_modal.php");
?>











 <!-- SIGN UP MODAL -->
<?php
require("./includes/index_signup_modal.php");
?>



<!--    TEST-->


<!--AGREEMENT MODAL-->
<?php
require("./includes/index_conditions_modal.php");
?>



<!-- JS / JQry  scripts -->
<?php
require("./includes/captcha.php");
require("./includes/jscripts.php");
?>



    <script type="text/javascript">

  function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
 </script>


        <script>

              $("#agreeBtn").click(function() {
                $("#conditionModal").modal('hide');
                $("#aplicantEmployerModal").modal('show');
            });
          </script>

</body>
</html>
