-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2016 at 12:34 PM
-- Server version: 10.1.6-MariaDB-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `samebffn_samers`
--

-- --------------------------------------------------------

--
-- Table structure for table `resumeDocs`
--

CREATE TABLE IF NOT EXISTS `resumeDocs` (
  `uid` int(100) NOT NULL AUTO_INCREMENT,
  `file` varchar(1000) NOT NULL,
  `type` varchar(1000) NOT NULL,
  `size` varchar(1000) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `samerAdmin`
--

CREATE TABLE IF NOT EXISTS `samerAdmin` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `securitycode` varchar(255) NOT NULL,
  `companyname` varchar(255) NOT NULL,
  `accounttype` varchar(255) NOT NULL,
  `employeerCompanyAddress` varchar(255) NOT NULL,
  `employeerCompanyContact` varchar(255) NOT NULL,
  `employeerCompanyDetailinformation` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `samerAdmin`
--

INSERT INTO `samerAdmin` (`uid`, `username`, `password`, `securitycode`, `companyname`, `accounttype`, `employeerCompanyAddress`, `employeerCompanyContact`, `employeerCompanyDetailinformation`) VALUES
(1, 'asd@asd.com', '7815696ecbf1c96e6894b779456d330e', 'f12486a7d440e1d9a72af5bbde423a14', 'asad', '0', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `samerAdminJobPost`
--

CREATE TABLE IF NOT EXISTS `samerAdminJobPost` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `employeerJobPostOpenDate` varchar(255) NOT NULL,
  `employeerJobPostCompanyName` varchar(255) NOT NULL,
  `employeerJobPostPosition` varchar(255) NOT NULL,
  `employeerJobPostContact` varchar(255) NOT NULL,
  `employeerJobPostExprienceRequired` varchar(255) NOT NULL,
  `employeerJobPostAdditionalInformation` varchar(255) NOT NULL,
  `employeerJobPostCloseDate` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `samerAdminJobPost`
--

INSERT INTO `samerAdminJobPost` (`uid`, `employeerJobPostOpenDate`, `employeerJobPostCompanyName`, `employeerJobPostPosition`, `employeerJobPostContact`, `employeerJobPostExprienceRequired`, `employeerJobPostAdditionalInformation`, `employeerJobPostCloseDate`) VALUES
(1, '2016-06-23', 'Asd', 'asd', 'asd', 'asd', 'asd', '06-23-2016');

-- --------------------------------------------------------

--
-- Table structure for table `samerApplicants`
--

CREATE TABLE IF NOT EXISTS `samerApplicants` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `applicantDetail` varchar(1000) NOT NULL,
  `applicantJobTitle` varchar(1000) NOT NULL,
  `applicantJobUid` varchar(10000) NOT NULL,
  `applicantEmail` varchar(1000) NOT NULL,
  `applicantName` varchar(1000) NOT NULL,
  `applicantUid` varchar(1000) NOT NULL,
  `applicantDateApply` varchar(1000) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `samerFeedback`
--

CREATE TABLE IF NOT EXISTS `samerFeedback` (
  `uid` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phonenumber` varchar(1000) NOT NULL,
  `message` varchar(1000) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `samerFeedback`
--

INSERT INTO `samerFeedback` (`uid`, `name`, `email`, `phonenumber`, `message`) VALUES
(1, 'fdajflasjfadslk', 'email@email.com', '09981412314', 'you fucking website is suck');

-- --------------------------------------------------------

--
-- Table structure for table `samerUser`
--

CREATE TABLE IF NOT EXISTS `samerUser` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `securitycode` varchar(255) NOT NULL,
  `accounttype` varchar(100) NOT NULL,
  `applicantName` varchar(255) NOT NULL,
  `applicantEmail` varchar(255) NOT NULL,
  `applicantContact` varchar(255) NOT NULL,
  `applicantAddress` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `samerUser`
--

INSERT INTO `samerUser` (`uid`, `username`, `password`, `securitycode`, `accounttype`, `applicantName`, `applicantEmail`, `applicantContact`, `applicantAddress`) VALUES
(1, 'amaba@yahoo.com', '90a6590d27bfcbb281d396bc1f68e460', '36958cab94c42a1b917795651e12c476', '1', '', '', '', ''),
(2, 'email@email.com', '4f64c9f81bb0d4ee969aaf7b4a5a6f40', '9c645b6c54d530bb12bbff560b6c505d', '1', 'fdsfad@email', 'email@email.com', '2342423432445', 'dsfdsgdfgsdfgdfs'),
(3, 'EMAIL@YAHOO.COM', '5a1eb393b4f22e4fc0850977fc974e1f', 'a3e1915aca92130eac82bbe5002aa44e', '1', 'EMAIL USER', 'EMAIL@YAHOO.COM', '0932924392394', 'DJFLKADSJFLKADSJFLKADSJFLAKS'),
(4, 'ylizecarriegrei@yahoo.com', '250f3aa70a82c0ff9ad15c95821352c4', 'b143f10a6b12e3cda37aadd4cfcd0887', '1', 'Elgy Grace', 'ylizecarriegrei@yahoo.com', '+639158933078', 'Olongapo City'),
(5, 'asd2@asd.com', '7815696ecbf1c96e6894b779456d330e', '24cd318d47fe0ebb0ba4552dd8ae0425', '1', 'asd', 'asd2@asd.com', '1231233123', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `samerUserResume`
--

CREATE TABLE IF NOT EXISTS `samerUserResume` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `applicantResumePersonalName` varchar(1000) NOT NULL,
  `applicantResumePersonalContact` varchar(1000) NOT NULL,
  `applicantResumePersonalEmail` varchar(1000) NOT NULL,
  `applicantResumePersonalAddress` varchar(1000) NOT NULL,
  `applicantResumePersonalBirthDate` varchar(1000) NOT NULL,
  `applicantResumePersonalAge` varchar(1000) NOT NULL,
  `applicantResumePersonalNationality` varchar(1000) NOT NULL,
  `applicantResumeEducationSchoolname` varchar(1000) NOT NULL,
  `applicantResumeEducationGradDate` varchar(1000) NOT NULL,
  `applicantResumeEducationQualification` varchar(1000) NOT NULL,
  `applicantResumeEducationFieldStudy` varchar(1000) NOT NULL,
  `applicantResumeEducationAdditionalinformation` varchar(1000) NOT NULL,
  `applicantResumeWorkSkills` varchar(1000) NOT NULL,
  `applicantResumeWorkPosition` varchar(1000) NOT NULL,
  `applicantResumeWorkCompanyName` varchar(1000) NOT NULL,
  `applicantResumeWorkDate` varchar(1000) NOT NULL,
  `applicantResumeWorkSpecialization` varchar(1000) NOT NULL,
  `applicantResumeWorkRole` varchar(1000) NOT NULL,
  `applicantResumeWorkCountry` varchar(1000) NOT NULL,
  `applicantResumeWorkState` varchar(1000) NOT NULL,
  `applicantResumeWorkIndustry` varchar(1000) NOT NULL,
  `applicantResumeWorkExprienceDescription` varchar(1000) NOT NULL,
  `applicantResumeAdditionalInfo` varchar(1000) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
